#!/usr/bin/env sh

COMMAND=$1

IMG_NAME=shortenurl-0.0


case $COMMAND in
    init)
        python -m venv venv
        source venv/bin/activate
        pip install -r requirements-dev.txt
        ;;
    build)
        docker build \
            -f Dockerfile --tag $IMG_NAME --no-cache .
        ;;
    debug)
        docker run --rm -it $IMG_NAME bash
        ;;
    run)
        PWD=$(pwd)
        docker run --rm -it \
            -p8000:80 \
            $IMG_NAME
        ;;
    *)
        echo "Usage:"
        echo " operation.sh init  # Initializes the dev env."
        echo " operation.sh build # build a new docker image."
        echo " operation.sh debug # allows you to run a container."
        echo " operation.sh run   # runs the app"
        exit 1
        ;;
esac

