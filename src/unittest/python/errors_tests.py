import unittest
from errors import RedirectionException, RedirectionUnableToCreateException, \
    RedirectionNotFoundException, RedirectionExpiredException, \
    RedirectionAlreadyExistsException, RedirectionConfigurationException


class ErrorTest(unittest.TestCase):
    def test_redirection_error_default_message(self):
        try:
            raise RedirectionException()
            self.fail('Should have raised an exception')
        except RedirectionException as redirection_error:
            self.assertEqual(redirection_error.message, 'Internal error')
            self.assertEqual(redirection_error.status, 500)
            self.assertIsNone(redirection_error.path_id)

    def test_redirection_error_with_message(self):
        try:
            raise RedirectionException('Some error')
            self.fail('Should have raised an exception')
        except RedirectionException as redirection_error:
            self.assertEqual(redirection_error.message, 'Some error')
            self.assertEqual(redirection_error.status, 500)
            self.assertIsNone(redirection_error.path_id)

    def test_redirection_unable_to_create_exception_defaults(self):
        try:
            raise RedirectionUnableToCreateException()
            self.fail('Should have raised an exception')
        except RedirectionUnableToCreateException as unable_to_create:
            self.assertEqual(unable_to_create.message,
                             'Unable to create, try again')
            self.assertEqual(unable_to_create.status, 503)
            self.assertIsNone(unable_to_create.path_id)

    def test_redirection_unable_to_create_exception_message(self):
        try:
            raise RedirectionUnableToCreateException('Some message')
            self.fail('Should have raised an exception')
        except RedirectionUnableToCreateException as unable_to_create:
            self.assertEqual(unable_to_create.message,
                             'Some message')
            self.assertEqual(unable_to_create.status, 503)
            self.assertIsNone(unable_to_create.path_id)

    def test_redirection_not_found_exception_defaults(self):
        try:
            raise RedirectionNotFoundException('path_id')
            self.fail('Should have raised an exception')
        except RedirectionNotFoundException as not_found:
            self.assertEqual(not_found.message,
                             'Redirection with id "path_id" not found')
            self.assertEqual(not_found.status, 404)
            self.assertEqual(not_found.path_id, 'path_id')

    def test_redirection_expired_exception(self):
        try:
            raise RedirectionExpiredException('path_id')
            self.fail('Should have raised an exception')
        except RedirectionExpiredException as expired:
            self.assertEqual(expired.message,
                             'Redirection with path "path_id" has expired')
            self.assertEqual(expired.status, 410)
            self.assertEqual(expired.path_id, 'path_id')

    def test_redirection_already_exists_exception(self):
        try:
            raise RedirectionAlreadyExistsException('path_id')
            self.fail('Should have raised an exception')
        except RedirectionAlreadyExistsException as already_exists:
            self.assertEqual(already_exists.message,
                             'Redirection with path "path_id" already exists')
            self.assertEqual(already_exists.status, 409)
            self.assertEqual(already_exists.path_id, 'path_id')

    def test_redirection_configuration_exception(self):
        try:
            raise RedirectionConfigurationException(200)
            self.fail('Should have raised an exception')
        except RedirectionConfigurationException as configuration_exc:
            self.assertEqual(configuration_exc.message,
                             'Invalid endpoint description configuration 200')
            self.assertEqual(configuration_exc.status_code, 200)
