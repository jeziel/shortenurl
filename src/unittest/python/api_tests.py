import unittest
from unittest.mock import patch
from fastapi.testclient import TestClient
from api import app
from models import RedirectionCreate


class TestCreateRedirectApi(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)
        self.url = '/management/redirection'

    def test_invalid_model_returns_422(self):
        invalid_payload = {'hi': 'hello'}
        response = self.client.post(self.url, json=invalid_payload)
        self.assertEqual(response.status_code, 422)
        # self.assertEqual(response.json(), {})

    @patch('time.time', return_value=111)
    def test_creating_a_redirection_returns_201(self, time_):
        payload = RedirectionCreate(
            path_id='hello123',
            destination='https://ddg.co')
        response = self.client.post(self.url, json=payload.dict())
        expected_payload = {
            'path_id': 'hello123',
            'destination': 'https://ddg.co',
            'created_on': 111,
            'active_until': None,
        }
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(), expected_payload)

    def test_an_invalid_destination_url_should_fail_with_validation(self):
        payload = {
            'destination': 'hs://hello',
        }
        response = self.client.post(self.url, json=payload)
        self.assertEqual(response.status_code, 422)
        # self.assertEqual(response.json(), {})

    def test_a_short_path_should_fail_with_validation(self):
        too_short = 'ab'
        payload = {
            'path_id': too_short,
            'destination': 'hs://hello',
        }
        response = self.client.post(self.url, json=payload)
        self.assertEqual(response.status_code, 422)
        # self.assertEqual(response.json(), {})

    def test_creating_a_redirection_with_an_existing_path_id_returns_409(self):
        existing_path_id = 'active'
        payload = RedirectionCreate(
            path_id=existing_path_id,
            destination='https://ddg.co')
        response = self.client.post(self.url, json=payload.dict())
        message = f'Redirection with path "{existing_path_id}" already exists'
        expected_payload = {
            'entity_id': existing_path_id,
            'message': message,
        }
        self.assertEqual(response.status_code, 409)
        self.assertEqual(response.json(), expected_payload)

    @patch('time.time', return_value=111)
    def test_not_providing_a_path_id_generates_one_when_creating(self, time_):
        payload = RedirectionCreate(destination='https://ddg.co')
        response = self.client.post(self.url, json=payload.dict())
        expected_payload = {
            'destination': 'https://ddg.co',
            'created_on': 111,
            'active_until': None,
        }

        self.assertEqual(response.status_code, 201)
        response_payload = response.json()
        self.assertIsNotNone(response_payload)
        self.assertTrue('path_id' in response_payload)
        self.assertIsNotNone(response_payload['path_id'])
        del response_payload['path_id']
        self.assertEqual(response_payload, expected_payload)

    def test_providing_a_bad_path_id_should_fail_validation(self):
        invalid_paths = ['hello.123', '', '   ', 'hello-world']
        for invalid_path in invalid_paths:
            payload = {
                'path_id': invalid_path,
                'destination': 'https://ddg.co'
            }
            response = self.client.post(self.url, json=payload)
            self.assertEqual(response.status_code, 422)


class TestGetRedirectApi(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)
        self.url = '/management/redirection'

    def get_url(self, redirect_id) -> str:
        return f'{self.url}/{redirect_id}'

    def test_requesting_a_non_existent_redirection_returns_404(self):
        nonexistent = 'nonexistent'
        response = self.client.get(self.get_url(nonexistent))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {
            'entity_id': nonexistent,
            'message': f'Redirection with id "{nonexistent}" not found',
        })

    def test_requesting_an_existing_redirection_returns_200(self):
        existing = 'active'
        response = self.client.get(self.get_url(existing))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'path_id': existing,
            'created_on': 1898989879,
            'destination': 'https://ddg.co',
            'active_until': None,
        })


class TestDeleteRedirectApi(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)
        self.url = '/management/redirection'

    def get_url(self, redirect_id) -> str:
        return f'{self.url}/{redirect_id}'

    def test_requesting_a_non_existent_redirection_returns_404(self):
        nonexistent = 'nonexistent'
        response = self.client.delete(self.get_url(nonexistent))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {
            'entity_id': nonexistent,
            'message': f'Redirection with id "{nonexistent}" not found',
        })

    def test_requesting_an_existing_redirection_returns_200(self):
        existing = 'gone'
        response = self.client.delete(self.get_url(existing))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(self.get_url(existing))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {
            'entity_id': existing,
            'message': f'Redirection with id "{existing}" not found',
        })


# TODO.
# These tests are important but couldn't get to a point of testing the different
# scenario. I'll come back to these ones later.
# 1. A valid redirection should take you to the destination URL
# 2. An expired redirection should take you to a /expired URL.
# 3. A nonexistent redirection should take you to / where they can create one.
class TestRedirectionApi(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    def test_should_redirect(self):
        response = self.client.get(
            '/active',
            allow_redirects=False)
        self.assertEqual(response.text, '')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.headers, {'location': 'https://ddg.co'})

    def test_should_be_expired(self):
        response = self.client.get(
            '/expired',
            headers={'Accept': 'text/html'},
            allow_redirects=False)
        self.assertEqual(response.status_code, 410)
        self.assertIn('has expired', response.text)

    def test_not_fond(self):
        response = self.client.get('/not_found', allow_redirects=False)
        self.assertEqual(response.status_code, 404)
        self.assertIn('not found', response.text)

    @patch('service.get_active_redirection_by_path_id', side_effect=Exception)
    def test_other_error(self, error_mock):
        response = self.client.get('/super_error', allow_redirects=False)
        self.assertEqual(response.status_code, 500)
        self.assertIn('error', response.text)
