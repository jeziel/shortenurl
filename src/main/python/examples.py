from typing import Dict
from models import RedirectionGet, RedirectionErrorContent, \
    RedirectionErrorDescription, RedirectionCreate
from errors import RedirectionConfigurationException


path_id = 'eAtcR'
time_stamp = 1636741397
url = 'https://www.gboards.ca'

redirection_get = RedirectionGet(
    path_id=path_id,
    created_on=time_stamp,
    active_until=time_stamp,
    destination=url,
).dict()

redirection_create = RedirectionCreate(
    path_id=path_id,
    active_until=time_stamp,
    destination=url,
).dict()


def get_error_example(status_code: int, message: str) -> RedirectionErrorDescription:
    redirection_error_content = RedirectionErrorContent(
        message=message,
        entity_id=path_id,
    )

    return RedirectionErrorDescription(
        status_code=status_code,
        content=redirection_error_content,
    ).dict()


_responses = {
    'redirection_get': redirection_get,
    'created': redirection_create,
    'not_found': get_error_example(404, 'Not found'),
    'conflict': get_error_example(409, 'Already exists'),
    'gone': get_error_example(410, 'Redirection has expired'),
}

_full_status_description = {
    200: {
        'description': 'Redirection was found',
        'content': {
            'application/json': {
                'example': _responses.get('redirection_get'),
            }
        }
    },
    201: {
        'description': 'Redirection was created',
        'content': {
            'application/json': {
                'example': _responses.get('redirection_get'),
            }
        }
    },
    404: {
        'description': 'The redirection was not found',
        'model': RedirectionErrorDescription,
        'content': {
            'application/json': {
                'example': _responses.get('not_found')
            }
        }
    },
    409: {
        'description': 'A redirection already exists',
        'model': RedirectionErrorDescription,
        'content': {
            'application/json': {
                'example': _responses.get('conflict'),
            }
        }
    },
    410: {
        'description': 'The redirection has already expired',
        'model': RedirectionErrorDescription,
        'content': {
            'application/json': {
                'example': _responses.get('gone'),
            }
        }
    },
}


def status_description(status_code: int) -> Dict:
    if status_code not in _full_status_description:
        raise RedirectionConfigurationException(status_code)

    return {status_code: _full_status_description.get(status_code)}


def response_description(*statuses: int) -> Dict:
    descriptions = {}
    for status in statuses:
        descriptions.update(status_description(status))

    return descriptions
