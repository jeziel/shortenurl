from typing import Optional
from pydantic import BaseModel, HttpUrl, validator, Field
from conf import conf


class RedirectionBase(BaseModel):
    path_id: str = Field(description='The redirection path')
    active_until: Optional[int] = Field(
        default=None, description='The date until this redirection will be valid')
    destination: HttpUrl = Field(description='The destination URL')

    @validator('path_id')
    def check_chars(cls, path_id: str) -> str:
        if not path_id or not path_id.isalnum():
            raise ValueError('Must be alphanumeric')

        min_length = conf.get('minimun_path_length')
        if len(path_id) < min_length:
            raise ValueError(f'Should be at least {min_length}')

        return path_id


class RedirectionRecord(RedirectionBase):
    created_on: int = Field(description='The date this redirection was created')


class Redirection(RedirectionBase):
    pass


class RedirectionCreate(Redirection):
    path_id: Optional[str] = Field(default=None, description='The redirection path')

    @validator('path_id')
    def check_chars(cls, path_id: Optional[str]) -> Optional[str]:
        if path_id is None:
            return None

        return super(RedirectionCreate, cls).check_chars(path_id)


class RedirectionGet(Redirection):
    created_on: int = Field(
        description='The timestamp this redirection was created')


class RedirectionErrorContent(BaseModel):
    message: str = Field(description='The reason for the error')
    entity_id: Optional[str] = Field(
        default='', description='The optional related entity for this error')


class RedirectionErrorDescription(BaseModel):
    status_code: int = Field(description='The status code of the request')
    content: RedirectionErrorContent = Field(
        description='Information about the error')
