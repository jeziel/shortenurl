from typing import Optional
from models import RedirectionRecord, RedirectionCreate
from tools import timestamp_now


# TODO. MySQL at some point? Not really sure if mysql would be the right database
# for this service, I'm thinking that a DynamoDB table should be enough, the only
# part that I'm not sure how it could work is if we wanted to have some sort of
# user entity that needs to be associated to the redirection record. In fact a
# DynamoDB table could use the TTL to remove the record at the correct time. We
# could take the record insert it into another table for a user to see what links
# have expired.

existing_redirections = {
    'active': {
        'path_id': 'active',
        'created_on': 1898989879,
        'destination': 'https://ddg.co'
    },
    'expired': {
        'path_id': 'expired',
        'created_on': 1898989879,
        'active_until': 15849,
        'destination': 'https://reddit.com'
    },
    'active_not_expired': {
        'path_id': 'active_not_expired',
        'created_on': 1898989879,
        'active_until': 1584956849,
        'destination': 'http://gboards.ca'
    },
    'gone': {
        'path_id': 'gone',
        'created_on': 1898989878,
        'active_until': 1584956848,
        'destination': 'http://mkultra.click'
    },
}


def get_redirection(path_id: str) -> Optional[RedirectionRecord]:
    redirection_data = existing_redirections.get(path_id)

    if not redirection_data:
        return None

    return RedirectionRecord(**redirection_data)


def create_redirection(redirection: RedirectionCreate) -> RedirectionRecord:
    record = RedirectionRecord(created_on=timestamp_now(), **redirection.dict())
    existing_redirections[redirection.path_id] = record.dict()

    return record


def delete_redirection(path_id: str) -> None:
    del existing_redirections[path_id]
