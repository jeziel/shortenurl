from typing import Optional
from fastapi import status


class RedirectionException(RuntimeError):
    def __init__(self, message: Optional[str] = 'Internal error'):
        RuntimeError.__init__(self, message)
        self.message = message
        self.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        self.path_id = None


class RedirectionUnableToCreateException(RedirectionException):
    def __init__(self, message: Optional[str] = 'Unable to create, try again'):
        RedirectionException.__init__(self, message)
        self.status = status.HTTP_503_SERVICE_UNAVAILABLE


class RedirectionNotFoundException(RedirectionException):
    def __init__(self, path_id: str):
        RedirectionException.__init__(
            self,
            message=f'Redirection with id "{path_id}" not found')
        self.path_id = path_id
        self.status = status.HTTP_404_NOT_FOUND


class RedirectionExpiredException(RedirectionException):
    def __init__(self, path_id: str):
        RedirectionException.__init__(
            self,
            f'Redirection with path "{path_id}" has expired')
        self.path_id = path_id
        self.status = status.HTTP_410_GONE


class RedirectionAlreadyExistsException(RedirectionException):
    def __init__(self, path_id: str):
        RedirectionException.__init__(
            self,
            f'Redirection with path "{path_id}" already exists')
        self.path_id = path_id
        self.status = status.HTTP_409_CONFLICT


class RedirectionConfigurationException(RuntimeError):
    def __init__(self, status_code: int):
        self.message = f'Invalid endpoint description configuration {status_code}'
        self.status_code = status_code
        RuntimeError.__init__(self, self.message)
