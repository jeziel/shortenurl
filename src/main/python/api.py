import logging
from fastapi import FastAPI, status, Request
from fastapi.responses import JSONResponse, RedirectResponse, HTMLResponse
from models import RedirectionCreate, RedirectionGet, \
    RedirectionErrorDescription, RedirectionErrorContent
from errors import RedirectionException, RedirectionNotFoundException, \
    RedirectionExpiredException
import service
from examples import response_description


app = FastAPI()
redirection_logger = logging.getLogger('api.redirection')
redirection_logger.setLevel(level=logging.WARN)


# TODO. try a different exception for a different handler. I guess for
# exceptions that come from the same app, this doesn't make sense, you
# can have for example a Redirect and a User type of exceptions but they
# will all be part of the same app RedirectionException.
@app.exception_handler(RedirectionException)
async def redirection_exception_handler(
        request: Request,
        exc: RedirectionException):
    error_details = RedirectionErrorDescription(
        status_code=exc.status,
        content=RedirectionErrorContent(
            message=exc.message,
            entity_id=exc.path_id,
        ))

    logging.warn('Unable to process request. %r', error_details.dict())

    return JSONResponse(**error_details.dict())


@app.post('/management/redirection',
          response_model=RedirectionGet,
          status_code=status.HTTP_201_CREATED,
          responses={
              **response_description(
                  status.HTTP_201_CREATED,
                  status.HTTP_409_CONFLICT
              ),
          })
async def create_redirection(redirection: RedirectionCreate) -> None:
    return service.create_redirection_record(redirection)


@app.get('/management/redirection/{path_id}',
         response_model=RedirectionGet,
         status_code=status.HTTP_200_OK,
         responses={
             **response_description(
                 status.HTTP_200_OK,
                 status.HTTP_404_NOT_FOUND
             ),
         })
async def get_redirection(path_id: str) -> RedirectionGet:
    return service.get_redirection(path_id)


@app.delete('/management/redirection/{path_id}',
            status_code=status.HTTP_200_OK,
            responses={
                **response_description(status.HTTP_404_NOT_FOUND),
            })
async def delete_redirection(path_id: str):
    service.delete_redirection(path_id)


@app.get('/{path_id}',
         status_code=status.HTTP_302_FOUND,
         responses={
             **response_description(
                 status.HTTP_404_NOT_FOUND,
                 status.HTTP_410_GONE
             ),
         })
async def get_redirected(path_id: str):
    try:
        # todo. would it be possible to somehow handle this exceptions in the
        # exception_handler and instead of returning JSONResponse return an
        # HTMLResponse?
        redirection = service.get_active_redirection_by_path_id(path_id)

        return RedirectResponse(
            url=redirection.destination,
            status_code=status.HTTP_302_FOUND)
    except RedirectionNotFoundException as rnfe:
        redirection_logger.info('Redirection not found %r' % path_id)
        html_content = "<html><body>not found</body></html>"
        return HTMLResponse(content=html_content, status_code=rnfe.status)
    except RedirectionExpiredException as ree:
        redirection_logger.info('Redirection expired %r' % path_id)
        html_content = "<html><body>redirection has expired.</body></html>"
        return HTMLResponse(content=html_content, status_code=ree.status)
    except Exception as exc:
        redirection_logger.error(
            'Unable to process request. %r' % exc, exc_info=True)
        html_content = "<html><body>Other error.</body></html>"
        return HTMLResponse(content=html_content,
                            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
