from functools import lru_cache
import time
from tools import generate_target
from models import RedirectionGet, RedirectionCreate
from errors import RedirectionNotFoundException, RedirectionExpiredException, \
    RedirectionAlreadyExistsException, RedirectionUnableToCreateException
import store
from conf import conf


def get_redirection(path_id: str) -> RedirectionGet:
    redirection = store.get_redirection(path_id)

    if redirection is None:
        raise RedirectionNotFoundException(path_id)

    return RedirectionGet(**redirection.dict())


# TODO. this is missing expiration. This will be a local per instance cache
@lru_cache(maxsize=1000)
def get_active_redirection_by_path_id(path_id: str) -> RedirectionGet:
    redirection = store.get_redirection(path_id)

    if redirection is None:
        raise RedirectionNotFoundException(path_id)

    current_timestamp = int(time.time())

    if redirection.active_until and current_timestamp > redirection.active_until:
        raise RedirectionExpiredException(path_id)

    return RedirectionGet(**redirection.dict())


def create_redirection_record(redirection: RedirectionCreate) -> RedirectionGet:
    if redirection.path_id is None:
        redirection.path_id = _create_new_path()
    else:
        _assert_redirection_does_not_exists(redirection.path_id)

    record = store.create_redirection(redirection)

    return record


def delete_redirection(path_id: str) -> None:
    redirection = store.get_redirection(path_id)
    if redirection is None:
        raise RedirectionNotFoundException(path_id)

    store.delete_redirection(path_id)


def _assert_redirection_does_not_exists(path_id: str) -> None:
    existing_redirection = store.get_redirection(path_id)
    if existing_redirection:
        raise RedirectionAlreadyExistsException(path_id)


def _create_new_path() -> str:
    max_attempts = conf.get('max_creation_attempts')
    path_id = None
    try_again = True
    counter = 0

    while try_again:
        path_id = generate_target(3 + counter)
        existing_redirection = store.get_redirection(path_id)

        counter += 1
        try_again = existing_redirection is not None and counter < max_attempts

    if path_id is None:
        raise RedirectionUnableToCreateException()

    return path_id
