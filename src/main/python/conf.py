
# this can be better, a few thoughts
# - the hardcoded values should come from the env, or some store (SSM?, Dynamo?)
#   where these can be managed.
# - the app should raise an exception when a conf attr is not found.
conf = {
    'minimun_path_length': 3,
    'max_creation_attempts': 20,
}
