import random
import string
import time
from conf import conf


chars = string.ascii_letters + string.digits
bad_words = []


def generate_target(length: int = 3) -> str:
    min_length = conf.get('minimun_path_length')
    if length < min_length:
        length = min_length

    # TODO. sanitize some possible bad words.
    return ''.join([random.choice(chars) for _ in range(length)])


def timestamp_now() -> int:
    return int(time.time())
