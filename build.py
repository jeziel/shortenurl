#   -*- coding: utf-8 -*-
from pybuilder.core import use_plugin, init, task


use_plugin("python.core")
use_plugin("python.unittest")
use_plugin("python.flake8")
use_plugin("python.coverage")


name = "shortenurl"
default_task = "publish"
branch_coverage = 80
branch_threshold_warn = 50


@init
def set_properties(project):
    project.depends_on_requirements("src/main/python/requirements.txt")
    project.build_depends_on_requirements("requirements-dev.txt")
    project.set_property('flake8_break_build', True)
    project.set_property('flake8_max_line_length', 20)
    project.set_property('flake8_verbose_output', True)
    project.set_property('coverage_threshold_warn', branch_coverage)
    project.set_property('coverage_branch_threshold_warn', branch_threshold_warn)
    project.set_property('ut_coverage_branch_threshold_warn', branch_coverage)
    project.set_property(
        'ut_coverage_branch_partial_threshold_warn', branch_threshold_warn)
