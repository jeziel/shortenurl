# Intro
I took this opportunity to give FastAPI and Pydantic a try, I think I could've
gone with flask but I might as well learn something new as part of this
exercise.

One thing that I like about FastAPI is the user of typings to declare payload.
I'm working on a node/Typescript project that takes the defined models and
publish them as part of swagger.

Once the API is running you can see the swagger documentation and "manage" the
redirections.

I wanted to divide the project in 3 different parts
1. The management APIs to create/delete, etc. The redirections.
2. The actual endpoint that will create do the redirection.
3. A static website with information about the app and a few resources that are
   relevant to redirection when they are not found or the redirection has
   expired.

I'm thinking that this will allow the different parts of the project to scale
as needed. I'm thinking that the heavy part will be 2. while 1. would see less
calls.

The plan of the management APIs is for an hypothetical dashboard that a user
would use to see the redirections that they have and add new ones, delete, etc.
There's no entity for user so for now this is an "free-for-all" API, as long as
you know the redirection you can remove it for example.

## Swagger
I like how the swagger documentation came as part of the code, it still has
a lot to be done, like better examples, description of what the different
status code mean, what the attributes are about, etc. It's still better than no
documentation. See below for the URL once the app is running.

# Dev set up
I didn't set up the local env with Docker, these are the steps you'll need to
run the test cases. I use, `pyenv` to set up my python version but if you have
python3.7 you should be good.

## Installing pybuilder
`pybuilder` is a tool to organize the build process of your project, runs tests
checks coverage, linting, etc. And it can even help with the creation of an
image, I didn't do that here. We need `pybuilder` to run the tests.

```
python -m venv venv
source venv/bin/activate
pip install pybuilder
```

Once `pybuilder` is installed, you can run the tests like: `pyb`.

# Running the app
I created a simple Dockerfile to package the app, here are the steps to do
that.

```
export IMG_NAME=shortenurl-0.0
docker build -f Dockerfile --tag $IMG_NAME --no-cache .
docker run -p8000:80 --rm -it $IMG_NAME
```

Once the app is running you can access the API or call the redirection
endpoint.

 - API: http://localhost:8000/docs#
 - Redirection endpoint: http://localhost:8000/{path_id}

There are a few "loaded" redirection records you can see them in `store.py`
- `http://localhost:8000/active`

# Deployment
I didn't work on deployment for the app, as I mentioned I wanted to have
3 different areas of the project. I like to have API instead of packages that
are shared between the different parts, but in this case it gives me the
impression that given the projected load of the redirection URL it would be
better to access storage directly instead of using an API endpoint.

In any case, I don't think it'll take too much to take the Dockerfile I have
and prepare to start an instance to serve the requests. I might try to use the
pipeline tool that github has.

At the place I'm working we use bitbucket and bitbucket pipelines to define the
build process and put an image in jfrog, from there bamboo is used to deploy
to the right environment, the reason for that is that bitbucket is a service we
don't manage and bamboo is self hosted.

I don't know if the best choice is to put this as a lambda, but that's how I'll
start.

I didn't dedicate any time defining the stack, but at a minimum it'd be the
lambda and the DB which given what I know so far, I would use DynamoDB as
a start and see how it performs. I may spend some time with
terraform/terragrunt and see if I can deploy it to my personal DigitalOcean
account, I'd need to use a droplet and one of those managed DBs they have. I'll
continue my ranting with the idea that I'll use DynamoDB.

I'm not concern about the amount of calls that dynamo will receive, there's a
index and that would speed things up. The size of the document is also not an
issue since it's pretty small, I think one thing that I'll limit is the size
of the destination URL because it's an open `str` field.

The only thing that I'd be concern is the amount of records that can get
added to the DB, given the limits that DynamoDB has in place, I also think that
it would take a long time to hit those limits? Also with the right policy we
could manage old redirections that can be removed since they were last used
a long time ago or never used?

Another thing that should be in place before this can be considered ready for
production would be supportability at this point the app just have some basic
logging in the FastAPI exception handler which is ok for error reporting. The
other thing is some DataDog monitoring to generate alerts or create dashboards,
for whatever can be considered observable, probably it can be the response
times and call count for the redirection endpoint and to a lesser extent the
management endpoints.

Another thing that should be in place is the analysis of dependencies with
respect to security patches, I believe there are some python project that are
checking that and can be incorporated in the build process.

# Notes
When adding GitLab CICD configuration, I started using the alpine image, but
had the following issue with it:

```
BUILD FAILED - Unable to install dependency packages into /target/venv/build/cpython-3.7.12.final.0. Please see '/target/logs/install_dependencies/venv_build_install_logs' for full details:
	        self.run_command(cmd_name)
	      File "/usr/local/lib/python3.7/distutils/cmd.py", line 313, in run_command
	        self.distribution.run_command(command)
	      File "/usr/local/lib/python3.7/distutils/dist.py", line 985, in run_command
	        cmd_obj.run()
	      File "/target/venv/build/cpython-3.7.12.final.0/lib/python3.7/site-packages/setuptools/command/build_ext.py", line 79, in run
	        _build_ext.run(self)
	      File "/usr/local/lib/python3.7/distutils/command/build_ext.py", line 340, in run
	        self.build_extensions()
	      File "/tmp/pip-install-dl8as1hb/uvloop_69c985de00a640d985accd42402a7a4f/setup.py", line 242, in build_extensions
	        self.build_libuv()
	      File "/tmp/pip-install-dl8as1hb/uvloop_69c985de00a640d985accd42402a7a4f/setup.py", line 223, in build_libuv
	        cwd=LIBUV_BUILD_DIR, env=env, check=True)
	      File "/usr/local/lib/python3.7/subprocess.py", line 512, in run
	        output=stdout, stderr=stderr)
	    subprocess.CalledProcessError: Command '['./configure']' returned non-zero exit status 1.
	    ----------------------------------------
	ERROR: Command errored out with exit status 1: /target/venv/build/cpython-3.7.12.final.0/bin/python -u -c 'import io, os, sys, setuptools, tokenize; sys.argv[0] = '"'"'/tmp/pip-install-dl8as1hb/uvloop_69c985de00a640d985accd42402a7a4f/setup.py'"'"'; __file__='"'"'/tmp/pip-install-dl8as1hb/uvloop_69c985de00a640d985accd42402a7a4f/setup.py'"'"';f = getattr(tokenize, '"'"'open'"'"', open)(__file__) if os.path.exists(__file__) else io.StringIO('"'"'from setuptools import setup; setup()'"'"');code = f.read().replace('"'"'\r\n'"'"', '"'"'\n'"'"');f.close();exec(compile(code, __file__, '"'"'exec'"'"'))' install --record /tmp/pip-record-64k17usg/install-record.txt --single-version-externally-managed --compile --install-headers /target/venv/build/cpython-3.7.12.final.0/include/site/python3.7/uvloop Check the logs for full command output.
	WARNING: You are using pip version 21.2.4; however, version 21.3.1 is available.
	You should consider upgrading via the '/target/venv/build/cpython-3.7.12.final.0/bin/python -m pip install --upgrade pip' command. (site-packages/pybuilder/install_utils.py:105)
```

In any case I read the following README from the author of FastAPI:

https://github.com/tiangolo/uvicorn-gunicorn-docker#-alpine-python-warning`
