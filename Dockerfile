FROM python:3.7

WORKDIR /app

COPY src/main/python/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY ./src/main/python .

CMD uvicorn api:app --host 0.0.0.0 --port 80
